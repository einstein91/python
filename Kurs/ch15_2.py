def sum_factorial(n):
	if n==1:
		return 1;
	else:
		return n + sum_factorial(n-1)

for n in range(1,101):
	print(n, ": ", sum_factorial(n))
	