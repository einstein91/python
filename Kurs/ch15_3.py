def pascal_triangle(n_rows):
    """ Creates the triangle of pascal """
    
    if n_rows == 1:
        return [1]
    else:
        line = [1]
        previous_line = pascal_triangle(n_rows-1)
        for l in range(len(previous_line)-1):
            line.append(previous_line[l] + previous_line[l+1])
        line += [1]
           
    return line
     
for l in range(1,20):
    print(pascal_triangle(l))    