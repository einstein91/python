from math import sqrt

def prime_number(cut_off):

    prime = list(range(2,cut_off+1)) #1. Liste erzeugen
    num = 2 #2
    max = sqrt(cut_off)
    
    while num<max:
        i = num
        while i<cut_off:
            i += num
            if i in prime:
                prime.remove(i)
        for j in prime:
            if j> num:
                num = j 
                break
    return prime
    
print(prime_number(6000))    